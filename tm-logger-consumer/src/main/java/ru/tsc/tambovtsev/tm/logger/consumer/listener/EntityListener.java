package ru.tsc.tambovtsev.tm.logger.consumer.listener;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.logger.consumer.service.LogService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public final class EntityListener implements MessageListener {

    @Autowired(required=false)
    private LogService logService;

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NonNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String json = textMessage.getText();
        System.out.println(json);
        logService.log(json);
    }

}
