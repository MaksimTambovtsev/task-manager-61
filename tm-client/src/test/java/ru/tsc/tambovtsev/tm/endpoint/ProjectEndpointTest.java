package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.tambovtsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.tambovtsev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.tambovtsev.tm.dto.request.*;
import ru.tsc.tambovtsev.tm.dto.response.UserLoginResponse;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.marker.SoapCategory;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();
    private IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();
    private String token = null;
    private String idProject = null;

    @Before
    public void createProject() {
        final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        token = userLoginResponse.getToken();
        projectEndpoint.createProject(new ProjectCreateRequest(
                token, "Project1", "Desc1")
        );
        idProject = projectEndpoint.listProject(new ProjectListRequest(token, null))
                .getProjects().get(0).getId();
    }

    @Test
    public void createProjectTest() {
        projectEndpoint.createProject(new ProjectCreateRequest(
                token, "Project2", "Desc2")
        );
        Assert.assertFalse(projectEndpoint.listProject(
                new ProjectListRequest(token, null)).getProjects().get(1).getName().isEmpty()
        );
    }

    @Test
    public void changeProjectStatusByIdTest() {
        @NotNull final Status statusBefore = projectEndpoint.listProject(new ProjectListRequest(token, null))
                .getProjects().get(0).getStatus();
        projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(token, idProject, Status.COMPLETED));
        Assert.assertNotEquals(statusBefore, projectEndpoint.listProject(new ProjectListRequest(token, null)).
                getProjects().get(0).getStatus()
        );
    }

    @Test
    public void removeProjectByIdTest() {
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, idProject));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(token, null)).
                getProjects()
        );
    }

    @Test
    public void updateProjectByIdTest() {
        @NotNull final String nameBefore = projectEndpoint.listProject(new ProjectListRequest(token, null))
                .getProjects().get(0).getName();
        projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(token, idProject, "qwerty", "23234"));
        Assert.assertNotEquals(nameBefore, projectEndpoint.listProject(new ProjectListRequest(token, null)).
                getProjects().get(0).getName()
        );
    }

    @Test
    public void showProjectTest() {
        Assert.assertNotNull(projectEndpoint.showProject(
                new ProjectShowByIdRequest(token, idProject)).getProject().getName()
        );
    }

    @Test
    public void listProjectTest() {
        Assert.assertNotNull(projectEndpoint.listProject(
                new ProjectListRequest(token, null)).getProjects()
        );
    }



}
