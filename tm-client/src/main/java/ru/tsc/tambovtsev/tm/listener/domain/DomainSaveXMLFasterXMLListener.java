package ru.tsc.tambovtsev.tm.listener.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.DataXmlSaveFasterXmlRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;

@Component
public class DomainSaveXMLFasterXMLListener extends AbstractDomainListener {

    @NotNull
    private final static String NAME = "save-xml-fasterxml";

    @NotNull
    private final static String DESCRIPTION = "Save projects, tasks and users in xml fasterxml";

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@domainSaveXMLFasterXMLListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        getDomainEndpoint().saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
