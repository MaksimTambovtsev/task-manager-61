package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.component.ISaltProvider;
import ru.tsc.tambovtsev.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
