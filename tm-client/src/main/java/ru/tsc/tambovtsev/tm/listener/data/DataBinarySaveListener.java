package ru.tsc.tambovtsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.DataBinarySaveRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;

@Component
public final class DataBinarySaveListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Save data to binary file.";

    @NotNull
    private final String NAME = "data-save-bin";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinarySaveListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA SAVE BINARY]");
        getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
