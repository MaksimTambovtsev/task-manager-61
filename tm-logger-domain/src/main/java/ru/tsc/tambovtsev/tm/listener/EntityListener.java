package ru.tsc.tambovtsev.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.logger.EntityLogDTO;
import ru.tsc.tambovtsev.tm.enumerated.EntityOperationType;
import ru.tsc.tambovtsev.tm.component.*;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static ru.tsc.tambovtsev.tm.enumerated.EntityOperationType.*;

@NoArgsConstructor
public class EntityListener {

    private static Consumer<EntityLogDTO> CONSUMER = null;

    public static Consumer<EntityLogDTO> getConsumer() {
        return CONSUMER;
    }

    public static void setConsumer(Consumer<EntityLogDTO> consumer) {
        EntityListener.CONSUMER = consumer;
    }

    @PostLoad
    public void postLoad(@NotNull final Object entity) {
        sendMessage(entity, POST_LOAD);
    }

    @PrePersist
    public void prePersist(@NotNull final Object entity) {
        sendMessage(entity, PRE_PERSIST);
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity) {
        sendMessage(entity, POST_PERSIST);
    }

    @PreRemove
    public void preRemove(@NotNull final Object entity) {
        sendMessage(entity, PRE_REMOVE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity) {
        sendMessage(entity, POST_REMOVE);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object entity) {
        sendMessage(entity, PRE_UPDATE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, POST_UPDATE);
    }

    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        if (CONSUMER == null) return;
        @NotNull final String className = entity.getClass().getSimpleName();
        CONSUMER.accept(new EntityLogDTO(className, entity, operationType.name()));
    }


}
