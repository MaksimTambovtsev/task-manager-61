package ru.tsc.tambovtsev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Session;
import ru.tsc.tambovtsev.tm.model.User;

public interface IAuthService {

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    Session validateToken(@Nullable String token);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    User check(@Nullable String login, @Nullable String password);

}

