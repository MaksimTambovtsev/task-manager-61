package ru.tsc.tambovtsev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.service.IDomainService;
import ru.tsc.tambovtsev.tm.api.service.ILoggerService;
import ru.tsc.tambovtsev.tm.api.service.property.IApplicationPropertyService;

public interface IServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IApplicationPropertyService getPropertyService();

}
