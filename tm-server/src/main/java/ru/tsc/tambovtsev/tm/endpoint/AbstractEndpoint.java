package ru.tsc.tambovtsev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.tambovtsev.tm.api.service.IDomainService;
import ru.tsc.tambovtsev.tm.api.service.dto.IAuthService;
import ru.tsc.tambovtsev.tm.api.service.dto.IServiceLocator;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserService;
import ru.tsc.tambovtsev.tm.dto.request.AbstractUserRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.exception.system.AccessDeniedException;
import ru.tsc.tambovtsev.tm.dto.model.SessionDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

@Getter
@Controller
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @Nullable
    @Autowired
    private IServiceLocator serviceLocator;

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private IDomainService domainService;

    protected SessionDTO check(@Nullable AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return authService.validateToken(token);
    }

    protected SessionDTO check(@Nullable AbstractUserRequest request, @Nullable Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final IUserService userService = serviceLocator.getUserServiceDTO();
        @NotNull final SessionDTO session = authService.validateToken(token);
        @NotNull final String userId = session.getUserId();
        @Nullable final UserDTO user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role roleUser = user.getRole();
        if (roleUser != role) throw new AccessDeniedException();
        return session;
    }

}
